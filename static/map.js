/* global define */
'use strict';

define(['leaflet', 'jquery'],
    function(L, $) {
        var map = L.map('map').setView([50.83906, 4.35308], 13);

        function displayPos(position) {
            /*
            Display a marker (and a circle) on a given position

            used to display the position given by the geolocation
            */
            var lat = position.coords.latitude;
            var lon = position.coords.longitude;
            map.setView([lat,lon], 15);
            var icon = L.MakiMarkers.icon({color: "#0033ff", size: "l", icon: "building"});
            L.marker([lat, lon], {icon: icon}).bindPopup("Vous êtes ici").addTo(map);
            var circle = L.circle([lat,lon], 50, {
                fillOpacity: 0.5
            }).addTo(map);
        }

        function posError(error) {
            /*
            Function used when the geolocation do not give a position
            */
            console.log('Error : the geolocation is not given');
            //todo?
        }

        function bmdeInit() {
            L.tileLayer('http://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png', {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
                maxZoom: 18,
                id: 'c4ptaincrunch.ka5engdh',
                accessToken: 'pk.eyJ1IjoiYzRwdGFpbmNydW5jaCIsImEiOiJUdWVRSENNIn0.qssi5TBLeBinBsXkZKiI6Q'
            }).addTo(map);

            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(displayPos,posError);
            }
        }

        map.bmdeInit = bmdeInit;

        return map;
    }
);
