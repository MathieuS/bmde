from django.contrib.admin import AdminSite


from maps.models import Marker
from maps.admin import MarkerAdmin

from django.contrib import admin
from reversion.models import Revision, Version

admin.site.register(Revision)
admin.site.register(Version)

class MarkersEditSite(AdminSite):
    site_header = 'BMDE markers administration'
    site_title = 'BMDE'
    index_title = 'BMDE administration'
    index_template = 'marker-edit/index.html'

markers_edit_site = MarkersEditSite(name='markers-edit-site')
markers_edit_site.register(Marker, MarkerAdmin)
