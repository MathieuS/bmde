from django.conf.urls import include, url
from django.contrib import admin, auth
from rest_framework import routers
from www.admin import markers_edit_site
from www import settings
from registration.forms import RegistrationFormUniqueEmail
from registration.backends.hmac.views import RegistrationView


import maps.views
import closet.views
import iframe.views
import www.views

router = routers.DefaultRouter()
router.register(r'markers', maps.views.MarkerViewSet)
router.register(r'categories', closet.views.CategoryViewSet)
router.register(r'subcategories', closet.views.SubcategoryViewSet)
router.register(r'iframes', iframe.views.IframeViewSet)

urlpatterns = [
    url(r'^$', www.views.index),
    url(r'^accounts/register/$',
        RegistrationView.as_view(
            form_class=RegistrationFormUniqueEmail
        ),
        name='registration_register',
    ),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'^csv/import/select/category/$', maps.views.csv_import_choose_cat, name='csv_import'),
    url(r'^csv/import/category/(?P<cat_id>[0-9]+)', maps.views.csv_import_for_cat, name='csv_import_for_cat'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^marker-edit/login/$', auth.views.login),
    url(r'^marker-edit/', include(markers_edit_site.urls)),
    url(r'^api/', include(router.urls)),
    url(r'^frame/', include('iframe.urls')),
    url('^api/subcategories/(?P<subcategory_id>.+)/markers-2$',
        maps.serializers.MakersBySubCategoriesList.as_view()),
    url('^api/markers/(?P<marker_id>.+)/history$', maps.views.MarkerHistoryView.as_view(), name='markers-history'),
]

if settings.DEBUG:
    from django.contrib.staticfiles import views
    urlpatterns += [url(r'^static/(?P<path>.*)$', views.serve)]
