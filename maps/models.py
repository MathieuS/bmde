from django.contrib.gis.db import models
from closet.models import Subcategory
from reversion.models import Version

class ResourceManagerType(models.Model):
    """ The type of the resource manager """
    code = models.CharField(max_length=25, primary_key=True)
    name = models.CharField(max_length=255)
    order = models.SmallIntegerField(null=True, default='0')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['order']


class Marker(models.Model):
    """ A point / resource on the map """
    name = models.CharField(max_length=255)
    position = models.PointField(geography=True, blank=False)
    comment = models.TextField(null=False, default="", blank=True)
    subcategories = models.ManyToManyField(Subcategory)
    web = models.URLField(default="", blank=True)
    phone = models.CharField(max_length=255, blank=True)
    address = models.CharField(max_length=1000, default="")
    email = models.EmailField(blank=True)
    # author = personne qui fait la derniere modif
    manager_type = models.ForeignKey(ResourceManagerType, null=True)
    public = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    objects = models.GeoManager()

    def __str__(self):
        return self.name

    @property
    def last_modified_dmY(self):
        return "{:%d/%m/%Y}".format(self.last_modified)

    @property
    def email_before_at(self):
        if self.email:
            return self.email.split('@')[0]
        return None

    def email_after_at(self):
        if self.email:
            return self.email.split('@')[1]
        return None

    @property
    def last_author(self):
        versions = Version.objects.get_for_object(self)
        if versions[0].revision.user:
            return versions[0].revision.user.username
        else:
            return None

    def get_history_for_json(self):
        """
        Return an array with the history of the changes of the marker. The
        returned array can easily be converted in json as it.
        """
        versions = Version.objects.get_for_object(self)
        print(versions)
        return [
            {
                'date_created': v.revision.date_created,
                'username': v.revision.user.username,
                'comment': v.revision.comment
            } if v.revision.user else {
                'date_created': v.revision.date_created,
                'username': None,
                'comment': v.revision.comment
            } for v in versions]

    @property
    def content(self):
        return self.comment

    @property
    def lat(self):
        return self.position.y

    @property
    def lon(self):
        return self.position.x

    @property
    def popup(self): #TO DELETE
        tpl = """<h5>{0.name}</h5>"""
        if self.address != "":
            tpl += "<em>Adresse</em> : {0.address}<br><br>"
        if self.phone != "":
            tpl += "<em>Téléphone</em> : {0.phone}<br><br>"
        if self.web != "":
            tpl += '<b><a target="_blank" href="{0.web}">Site web</a></b><br><br>'

        tpl += "{0.comment}<br><br>"

        tpl += '<a href="http://dewey.be/contact.html" target="_blank">Signaler un problème</a>'

        return tpl.format(self)
