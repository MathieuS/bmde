FROM python:3

WORKDIR /var/www/bmde
COPY . /var/www/bmde

RUN apt-get update && apt-get -y install binutils libproj-dev gdal-bin

RUN pip3 install -r requirements.txt
